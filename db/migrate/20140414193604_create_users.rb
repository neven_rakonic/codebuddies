class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username, limit: 25
      t.string :github
      t.string :bitbucket
      t.string :picture

      t.timestamps
    end
    add_index :users, :username, unique: true
  end
end
