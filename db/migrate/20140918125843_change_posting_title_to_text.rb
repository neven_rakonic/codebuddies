class ChangePostingTitleToText < ActiveRecord::Migration
  def up
    change_column :postings, :description, :text
  end

  def down
    change_column :postings, :description, :string
  end
end
