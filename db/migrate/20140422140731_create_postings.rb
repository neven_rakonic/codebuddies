class CreatePostings < ActiveRecord::Migration
  def change
    create_table :postings do |t|
      t.string :title, limit: 60
      t.string :description

      t.timestamps
    end
  end
end
