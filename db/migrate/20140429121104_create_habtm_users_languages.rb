class CreateHabtmUsersLanguages < ActiveRecord::Migration
  def change
    create_table :users_languages, id: false do |t|
      t.belongs_to :users
      t.belongs_to :languages
    end
  end
end
