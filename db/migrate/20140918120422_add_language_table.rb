class AddLanguageTable < ActiveRecord::Migration
  def up
    create_table "languages", force: true do |t|
      t.string   "title"

      t.timestamps
    end
  end

  def down
    drop_table "languages"
  end
end
