class AddPostingLanguageHabtm < ActiveRecord::Migration
  def change
    create_join_table :postings, :languages, table_name: :postings_languages
  end
end
