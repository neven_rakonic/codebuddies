# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
example_languages = ['c', 'c++', 'c#', 'php', 'python', 'java', 'javascript', 'asp.net']
example_languages.each do |l|
  Language.create(title: l)
end
#Creates an admin
User.create(username: "admin",
            email: "admin@example.com",
            password: "12345678",
            password_confirmation: "12345678",
            github: "github",
            bitbucket: "bitbucket",
            about_me: "Yolo",
            admin: true)

#Creates a regular user
User.create(username: "user",
            email: "user@example.com",
            password: "12345678",
            password_confirmation: "12345678",
            github: "github",
            bitbucket: "bitbucket",
            about_me: "Yolo",
            admin: true)

30.times do |n|
  username = Faker::Name.name
  email = "random-mail#{n}@example.com"
  password = "12345678"
  github = "http://github.com/fakedude#{n}"
  bitbucket = "http://bitbucket.org/fakedude#{n}"
  about_me = Faker::Lorem.sentence(5)
  User.create(username: username,
              email: email,
              password: password,
              password_confirmation: password,
              github: github,
              bitbucket: bitbucket,
              about_me: about_me)
end

300.times do
  title = Faker::Lorem.words(Random.new.rand(1..4)).join(" ")
  description = Faker::Lorem.sentences(Random.new.rand(1..9)).join(" ")
  user_id = Random.new.rand(1..30)  
  posting = Posting.new(title: title,
              description: description,
              user_id: user_id)
  languages_for_posting = example_languages.sample(Random.new.rand(1..5))
  languages_for_posting.each do |l|
    posting.languages << Language.where(title: l)
  end
  posting.save
end