Codebuddies::Application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  # Custom controller so I can override default controllers from devise
  devise_for :users, :controllers => {:registrations => "registrations"}
  # Custom resources that add to default devise routes, creates nested routes
  resources :users, only: [:show, :index], :shallow => true do
    resources :postings
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root 'static#start'
  match '/postings',                        to: 'postings#all',                   via: 'get'
  match '/remote_languages',                to: 'users#remote_languages_edit',    via: 'patch'
  match '/remote_languages',                to: 'users#remote_languages_delete',  via: 'delete'

end
