require 'spec_helper'

describe "Navbar" do

  subject { page }
  describe "Navbar" do

    before { visit root_path }

    describe "displays home link" do
      it { should have_link('Home', root_path) }
    end
  end
end
