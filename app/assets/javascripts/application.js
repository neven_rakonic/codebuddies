// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require_tree .
//= require turbolinks

$(function() {
    /* this object contains useful sitewide information */
    var Site = {};
    Site.width = $(window).width();
    Site.breakpoint = 768;

    /* navigation related objects and selectors */
    var Nav = {};
    Nav.menuButton = $("#nav_menu")
    Nav.listItems = $("#navigation li").not(":first-child");


    /* called when site is loaded */

    Nav.checkNav = function() {
      /* the list items are hidden only if media query shows the menu button.
         using Site.width is unreliable due to the difference in how browsers
         calculate viewport width */
      if (Nav.menuButton.css("display") === "block") {
        Nav.listItems.hide();
      }
      else {
        Nav.listItems.show();
      }
    }

    /* always called when page is loaded */
    Nav.checkNav();

    /* event handlers */
    Nav.menuButton.click(function() {
      Nav.listItems.toggle();
    });

    /* makes sure Site.width is up to date */
    $( window ).resize(function() {
      if ($(window).width() !== Site.width){
        Site.width = $(window).width();
        Nav.checkNav();
        console.log(Site.width);
      }
    });


    /* used to apply custom css to select, input etc. */
    $('.cus').customSelect();
});




