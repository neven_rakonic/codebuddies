class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :postings
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates_uniqueness_of :username


  # sets default pagiantion per page
  self.per_page = 5

  def admin?
    admin
  end

  def get_username
    "#{user.username}"
  end

  def self.find_user_postings(user_id)
    self.find(user_id).postings.order('updated_at DESC')
  end

  scope :find_username, -> search  { where('username Like ?', "%#{search}%") }
end
