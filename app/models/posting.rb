class Posting < ActiveRecord::Base
  belongs_to :user
  validates :user, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :languages, presence: true
  validates_associated :user
  has_and_belongs_to_many :languages, join_table: :postings_languages

  self.per_page = 10

  default_scope { order('updated_at DESC') }

  def self.add_or_create_languages(selected, posting)
      #expected input is a string of languages, uniq makes sure there are no duplicate tags
      selected = selected.split(',').collect { |string| string.strip.downcase }
      selected.uniq!
      #if there are no params we skip this part and the validation takes care of the rest
      selected.each { |l| posting.languages << Language.find_or_create_by(title: l) } unless selected == nil
  end 

  def self.fuzzy_titles(title)
      self.where('title Like ?', "%#{title}%")
  end

  def self.postings_by_language(title)
      self.joins(:languages).where('languages.title' => title)
  end
end
