class Ability
  include CanCan::Ability

  def initialize(user)    
    user ||= User.new
    if user.admin?
        can :manage, :all
        can :access, :rails_admin
    end
    can [:read, :all], Posting
    can [:new, :create, :edit, :update, :destroy], Posting, :user_id => user.id
    can [:show, :index], User
  end
end
