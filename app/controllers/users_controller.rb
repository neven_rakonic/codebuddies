class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:show]
  load_and_authorize_resource
  def show
  end

  def index
    @users = User.find_username(params[:search]).page(params[:page])
  end
end