class RegistrationsController < Devise::RegistrationsController


  protected

  #redirects user to edit profile after sign up,
  #:new_user => true is used to display a welcoming message
  def after_sign_up_path_for(resource)
    url_for(action: 'edit', controller: 'registrations', :params => {:new_user=>true})
  end
end