class PostingsController < ApplicationController
  before_action :authenticate_user!, :except => :all
  load_and_authorize_resource :except => :create

  def index        
    @postings = User.find_user_postings(params[:user_id]).page(params[:page])
  end

  def new
    @languages = Language.all
  end

  def create
    @posting = current_user.postings.new(params.require(:posting).permit(:title, :description))
    Posting.add_or_create_languages(params[:posting_languages], @posting)
    create_save(DEFAULT_WARNING)
  end

  def all
    query = params[:search]
    query_tipe = params[:search_type]
    flash[:warning] = "Your search didn't match any results"
    
    if query_tipe == 'Title'
      @postings = @postings.fuzzy_titles(query)
    elsif query_tipe == 'Language'
      @postings = @postings.postings_by_language(query)
    end
    @postings = @postings.page(params[:page])    
  end

  def edit
    @known_languages = ''
    @posting.languages.each_with_index do |l, i|
      if i == 0
        @known_languages += l.title
      else
        @known_languages += ", #{l.title}"
      end
    end
  end

  def update
    @posting.update(title: params[:posting][:title], description: params[:posting][:description])
    @posting.languages.delete(:all)
    Posting.add_or_create_languages(params[:posting_languages], @posting)
    update_save(DEFAULT_WARNING)
  end

  def destroy
    #needs a redirect in case something goes wrong
    Posting.destroy(params[:id])
    redirect_to user_postings_path(current_user.id)
  end

  private

    DEFAULT_WARNING = "Please make sure you've filled all the fields!"

    def create_save(warning)
      if @posting.save
        redirect_to user_postings_path(current_user.id)
      else
        flash[:notice] = warning
        redirect_to new_user_posting_path(current_user.id)
      end
    end

    def update_save(warning)
      if @posting.save
        redirect_to user_postings_path(current_user.id)
      else
        flash[:notice] = warning
        redirect_to new_user_posting_path(params[:id])
      end

    end
end